# RedAmp task, read more in docs.pdf
# Author: Marek Linner
# GitLab: https://gitlab.com/Murmally/redamp-task


import psycopg2
import psycopg2.extras
import urllib.request
import os



# connect to database as default user, password is stored separetely
def connect_to_database(user = "postgres", host = "127.0.0.1", 
												port = "5432", database = "RedAmp"):
	try:
		password = open("pwd.txt", "r").read().strip()
		connection = psycopg2.connect(user = user, password = password,
																	host = host, port = port,
																	database = database)

		cursor = connection.cursor()
	except (Exception, psycopg2.Error) as error:
		print("An error occured while connecting to database.")
		print(error)

	return connection, cursor


# function returns a tuple of SQL statements to create tables
def construct_create_table_statements():
	statements = ("""
				CREATE TABLE IF NOT EXISTS sources (
					id SERIAL PRIMARY KEY,
					description TEXT UNIQUE
				);
			""","""
				CREATE TABLE IF NOT EXISTS ip_addresses (
					id SERIAL PRIMARY KEY,
					source_id INTEGER REFERENCES sources(id),
					address INET UNIQUE NOT NULL,
					address_version INTEGER,
					pulses INTEGER,
					description TEXT,
					country VARCHAR(4),
					city TEXT,
					latitude REAL,
					longitude REAL
				);
			""","""
				CREATE TABLE IF NOT EXISTS URLs (
					id SERIAL PRIMARY KEY,
					source_id INTEGER REFERENCES sources(id),
					URL TEXT UNIQUE NOT NULL
				);
			"""
		)
	return statements


# uses statements from construct_create_table_statements and executes them
def create_tables(connection, cursor):
	statements = construct_create_table_statements()
	try:
		for statement in statements:
			cursor.execute(statement)

		connection.commit()
	except (Exception, psycopg2.DatabaseError) as error:
		print("Error occured while creating tables.\n")
		print(error)


# reads both documents containing data sources and returns them
def get_data_sources():
	with open("URL_sources.txt") as file:
		URL_records = file.readlines()

	with open("IP_sources.txt") as file:
		IP_records = file.readlines()

	source_records = URL_records + IP_records
	return source_records


# inserts data sources from files to database
def insert_to_data_sources(connection, cursor):
	try:
		statement = """INSERT INTO sources(id, description) 
									VALUES (DEFAULT, %s) 
									ON CONFLICT (description) DO NOTHING;"""
		for record in get_data_sources():
			cursor.execute(statement, (record.strip(),))		

		connection.commit()
	except (Exception, psycopg2.Error) as error:
		print("Error occured during insertion to 'sources' table.")
		print(error)


# returns ID assigned to particular data source
def get_data_sources_id(source, cursor):
	try:
		query = "SELECT id FROM sources WHERE description = '%s'" % source
		cursor.execute(query)
		result = cursor.fetchone()
	except (Exception) as error:
		print("Error occured while getting source id.")
		print(error)
	return result


# downloads URL addresses from a given URL source
def download_URLs(address):
	try:
		# get our URLs
		urllib.request.urlretrieve(address, "URLs.txt")
	except (Exception) as error:
		print("Error occured while fetching from", address)
		print(error)

	URLs = open("URLs.txt").readlines()
	for URL in URLs:
		URL = URL.strip()

	os.remove("URLs.txt")
	return URLs


# inserts URLs to appropriate database table
def insert_to_URLs(source, connection, cursor, local = False):
	if local:
		URLs = open(source).readlines()
	else:
		URLs = download_URLs(source)

	source_id = get_data_sources_id(source, cursor)
	values = []
	for elem in URLs:
		values.append((source_id, elem))

	statement = """INSERT INTO URLs(id, source_id, URL) VALUES (DEFAULT, %s, %s)
								ON CONFLICT (URL) DO NOTHING;"""
	cursor.executemany(statement, values)
	connection.commit()
	return


# downloads IP addresses from a given data source
def download_ip_addresses(address):
	try:
		urllib.request.urlretrieve(address, "IPs.txt")
	except (Exception) as error:
		print("Error occured while retrieving IP addresses from", address)
		print(error)

	IPs = open("IPs.txt").readlines()
	for IP in IPs:
		IP = IP.strip()

	os.remove("IPs.txt")
	return IPs


# returns whether IP address is IPv4 of IPv6
def get_ip_address_version(address):
	if ":" in address:
		return 6
	else:
		return 4


# returns tuple of values to be inserted from badapis
def build_badapis_values(source, records, cursor):
	version, values = get_ip_address_version(records[0]), []
	source_id = get_data_sources_id(source, cursor)
	for record in records:
		values.append((source_id, psycopg2.extras.Inet(record.strip()), version))

	return values


# returns tuple of values to be inserted from alienvault
def build_alienvault_values(source, records, cursor):
	source_id, values = get_data_sources_id(source, cursor), []
	for record in records:
		# alienvault example:
		# 2.81.219.150#4#3#Malicious Host#PT#Braga#41.5503005981,-8.4201002121#3
		# IP # version # pulses # description # country # city # latitude,longitude
		line = record.split("#")
		coords = line[6].split(",")
		values.append((source_id, psycopg2.extras.Inet(line[0]), line[1], line[2],
		 								line[3], line[4], line[5], coords[0], coords[1]))

	return values


# inserts to table ip_addresses
# so far limited only to badapis and alienvault
def insert_to_ip_addresses(source, connection, cursor, local = False):
	if local:
		records = open(source).readlines()
	else:
		records = download_ip_addresses(source)

	if len(records[0].split("#")) > 1 and "alienvault" in source:
		statement = """INSERT INTO ip_addresses(id, source_id, address, address_version,
									 pulses, description, country, city, latitude, longitude) 
									 VALUES (DEFAULT, %s, %s, %s, %s, %s, %s, %s, %s, %s)
									 ON CONFLICT (address) DO NOTHING;"""

		values = build_alienvault_values(source, records, cursor)
	else:
		statement = """INSERT INTO ip_addresses(id, source_id, address, address_version) 
									VALUES (DEFAULT, %s, %s, %s) ON CONFLICT (address) DO NOTHING;"""

		values = build_badapis_values(source, records, cursor)

	cursor.executemany(statement, values)
	connection.commit()



# terminates connection to database, closes cursor and connection
def terminate_connection(connection, cursor):
 	if(connection):
 		cursor.close()
 		connection.close()


# the main method of this script
def main():
	connection, cursor = connect_to_database()
	create_tables(connection, cursor)
	insert_to_data_sources(connection, cursor)

	# insert to URLs for each URL source
	for URL_source in open("URL_sources.txt").readlines():
		insert_to_URLs(URL_source.strip(), connection, cursor)

	# insert to ip_addresses for each IP source
	for IP_source in open("IP_sources.txt").readlines():
		insert_to_ip_addresses(IP_source.strip(), connection, cursor)


	terminate_connection(connection, cursor)
	exit(0)


main()


